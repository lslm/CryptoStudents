CryptoStudents: Keep your academic life organized
===============================================================================
    AUTHOR: Lucas Santos
    PROGRAMMER(S): Lucas Santos
    LICENSE: MIT
-------------------------------------------------------------------------------
Manage your academic life using this simple tool. Get your subjects, tests and homeworks under control and keep your graduation organized.